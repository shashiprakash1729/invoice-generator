# Set the base image to node:alpine
FROM node:alpine
# Specify where our app will live in the container
WORKDIR /app

COPY . /app

#install dependencies
RUN npm install
RUN npm install react-scripts@5.0.1 -g
# We want the production version
RUN npm run build

# Prepare nginx
FROM nginx:alpine
COPY --from=0 /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d


# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
